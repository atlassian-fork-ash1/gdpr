-- Type        : update
-- Origin      : Jira mobile plugin
-- Description : mobile notification settings for user
-- Table valid only for specific versions : Jira>=8.3, Jira ServiceDesk>=4.3.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_0A5972_NOTIFICATION_SETTING.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_0A5972_NOTIFICATION_SETTING where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_0A5972_NOTIFICATION_SETTING set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : Jira mobile plugin
-- Description : configuration for push messages
-- Table valid only for specific versions : Jira>=8.3, Jira ServiceDesk>=4.3.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_0A5972_PUSH_REGISTRATION.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_0A5972_PUSH_REGISTRATION where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_0A5972_PUSH_REGISTRATION set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : atlassian whisper plugin
-- Description : properties for messaging campaigns
-- Table valid only for specific versions : Jira>=7.4, Jira ServiceDesk>=3.6.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_21F425_USER_PROPERTY_AO.*,USER as USER_before,'<NEW_PD_VALUE>' as USER_after from AO_21F425_USER_PROPERTY_AO where LOWER(USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_21F425_USER_PROPERTY_AO set USER = '<NEW_PD_VALUE>' where LOWER(USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Table valid only for specific versions : Jira>=7.6 (with Atlassian Troubleshooting and Support Tools up to version 1.10.5), Jira ServiceDesk>=3.9.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_2F1435_READ_NOTIFICATIONS.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_2F1435_READ_NOTIFICATIONS where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2F1435_READ_NOTIFICATIONS set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Table valid only for specific versions : Jira>=7.6 (with Atlassian Troubleshooting and Support Tools from version 1.10.5), Jira ServiceDesk>=3.9.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_4789DD_READ_NOTIFICATIONS.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_4789DD_READ_NOTIFICATIONS where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_4789DD_READ_NOTIFICATIONS set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_4AEACD_WEBHOOK_DAO.*,LAST_UPDATED_USER as LAST_UPDATED_USER_before,'<NEW_PD_VALUE>' as LAST_UPDATED_USER_after from AO_4AEACD_WEBHOOK_DAO where LOWER(LAST_UPDATED_USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_4AEACD_WEBHOOK_DAO set LAST_UPDATED_USER = '<NEW_PD_VALUE>' where LOWER(LAST_UPDATED_USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTIVITY_ENTITY.*,POSTER as POSTER_before,'<NEW_PD_VALUE>' as POSTER_after from AO_563AEE_ACTIVITY_ENTITY where LOWER(POSTER) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_563AEE_ACTIVITY_ENTITY set POSTER = '<NEW_PD_VALUE>' where LOWER(POSTER) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTIVITY_ENTITY.*,USERNAME as USERNAME_before,'<NEW_PD_VALUE>' as USERNAME_after from AO_563AEE_ACTIVITY_ENTITY where LOWER(USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_563AEE_ACTIVITY_ENTITY set USERNAME = '<NEW_PD_VALUE>' where LOWER(USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTOR_ENTITY.*,USERNAME as USERNAME_before,'<NEW_PD_VALUE>' as USERNAME_after from AO_563AEE_ACTOR_ENTITY where LOWER(USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_563AEE_ACTOR_ENTITY set USERNAME = '<NEW_PD_VALUE>' where LOWER(USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : hipchat integration plugin
-- Description : seems to be auth data for specific users - ability to use private rooms in hipchat
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_5FB9D7_AOHIP_CHAT_USER.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_5FB9D7_AOHIP_CHAT_USER where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_5FB9D7_AOHIP_CHAT_USER set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira software
-- Description : spring audit log (reopen/close sprint)
-- Table valid only for specific product : Jira Software
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_60DB71_AUDITENTRY.*,USER as USER_before,'<NEW_PD_VALUE>' as USER_after from AO_60DB71_AUDITENTRY where LOWER(USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_60DB71_AUDITENTRY set USER = '<NEW_PD_VALUE>' where LOWER(USER) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira software
-- Description : board admins
-- Table valid only for specific product : Jira Software
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_BOARDADMINS.*,`KEY` as KEY_before,'<NEW_PD_VALUE>' as KEY_after from AO_60DB71_BOARDADMINS where (LOWER(`KEY`) = LOWER('<CURRENT_PD_VALUE>') ) AND TYPE  = 'USER';

-- + UPDATE (be careful)
update AO_60DB71_BOARDADMINS set `KEY` = '<NEW_PD_VALUE>' where (LOWER(`KEY`) = LOWER('<CURRENT_PD_VALUE>') ) AND TYPE  = 'USER';

-- Type        : update
-- Origin      : jira software
-- Description : board
-- Table valid only for specific product : Jira Software
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_RAPIDVIEW.*,OWNER_USER_NAME as OWNER_USER_NAME_before,'<NEW_PD_VALUE>' as OWNER_USER_NAME_after from AO_60DB71_RAPIDVIEW where LOWER(OWNER_USER_NAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_60DB71_RAPIDVIEW set OWNER_USER_NAME = '<NEW_PD_VALUE>' where LOWER(OWNER_USER_NAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : Jira inform plugin
-- Description : event recipient
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_733371_EVENT_RECIPIENT.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_733371_EVENT_RECIPIENT where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_733371_EVENT_RECIPIENT set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : Jira inform plugin
-- Description : saved issue events
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_733371_EVENT.*,USER_KEY as USER_KEY_before,'<NEW_PD_VALUE>' as USER_KEY_after from AO_733371_EVENT where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_733371_EVENT set USER_KEY = '<NEW_PD_VALUE>' where LOWER(USER_KEY) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : invite users plugin
-- Description : invitation to jira
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,SENDER_USERNAME as SENDER_USERNAME_before,'<NEW_PD_VALUE>' as SENDER_USERNAME_after from AO_97EDAB_USERINVITATION where LOWER(SENDER_USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set SENDER_USERNAME = '<NEW_PD_VALUE>' where LOWER(SENDER_USERNAME) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : application user
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${lower_user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select app_user.*,user_key as user_key_before,'<NEW_PD_VALUE>' as user_key_after from app_user where LOWER(user_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update app_user set user_key = '<NEW_PD_VALUE>' where LOWER(user_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select audit_log.*,author_key as author_key_before,'<NEW_PD_VALUE>' as author_key_after,search_field as search_field_before,replace(search_field,'<CURRENT_PD_VALUE>','<NEW_PD_VALUE>') as search_field_after from audit_log where LOWER(author_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update audit_log set author_key = '<NEW_PD_VALUE>',search_field = replace(search_field,'<CURRENT_PD_VALUE>','<NEW_PD_VALUE>') where LOWER(author_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select audit_log.*,object_id as object_id_before,'<NEW_PD_VALUE>' as object_id_after from audit_log where (LOWER(object_id) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER';

-- + UPDATE (be careful)
update audit_log set object_id = '<NEW_PD_VALUE>' where (LOWER(object_id) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER';

-- Type        : update
-- Origin      : jira
-- Description : avatar
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/ViewProfile.jspa?name=${user_key}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select avatar.*,owner as owner_before,'<NEW_PD_VALUE>' as owner_after from avatar where LOWER(owner) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update avatar set owner = '<NEW_PD_VALUE>' where LOWER(owner) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue history
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select changegroup.*,author as author_before,'<NEW_PD_VALUE>' as author_after from changegroup where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update changegroup set author = '<NEW_PD_VALUE>' where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : columns in issue table view
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select columnlayout.*,username as username_before,'<NEW_PD_VALUE>' as username_after from columnlayout where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update columnlayout set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : component
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select component.*,lead as lead_before,'<NEW_PD_VALUE>' as lead_after from component where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update component set lead = '<NEW_PD_VALUE>' where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select draftworkflowscheme.*,last_modified_user as last_modified_user_before,'<NEW_PD_VALUE>' as last_modified_user_after from draftworkflowscheme where LOWER(last_modified_user) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update draftworkflowscheme set last_modified_user = '<NEW_PD_VALUE>' where LOWER(last_modified_user) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : favourite jql filter
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select favouriteassociations.*,username as username_before,'<NEW_PD_VALUE>' as username_after from favouriteassociations where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update favouriteassociations set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : attachment
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Attachments' sections
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fileattachment.*,author as author_before,'<NEW_PD_VALUE>' as author_after from fileattachment where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update fileattachment set author = '<NEW_PD_VALUE>' where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : send list of issues matching jql
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select filtersubscription.*,username as username_before,'<NEW_PD_VALUE>' as username_after from filtersubscription where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update filtersubscription set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : comment
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraaction.*,author as author_before,'<NEW_PD_VALUE>' as author_after from jiraaction where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraaction set author = '<NEW_PD_VALUE>' where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : comment
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraaction.*,updateauthor as updateauthor_before,'<NEW_PD_VALUE>' as updateauthor_after from jiraaction where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraaction set updateauthor = '<NEW_PD_VALUE>' where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,archivedby as archivedby_before,'<NEW_PD_VALUE>' as archivedby_after from jiraissue where LOWER(archivedby) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraissue set archivedby = '<NEW_PD_VALUE>' where LOWER(archivedby) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,assignee as assignee_before,'<NEW_PD_VALUE>' as assignee_after from jiraissue where LOWER(assignee) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraissue set assignee = '<NEW_PD_VALUE>' where LOWER(assignee) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,creator as creator_before,'<NEW_PD_VALUE>' as creator_after from jiraissue where LOWER(creator) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraissue set creator = '<NEW_PD_VALUE>' where LOWER(creator) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,reporter as reporter_before,'<NEW_PD_VALUE>' as reporter_after from jiraissue where LOWER(reporter) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update jiraissue set reporter = '<NEW_PD_VALUE>' where LOWER(reporter) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : dashboard
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select portalpage.*,username as username_before,'<NEW_PD_VALUE>' as username_after from portalpage where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update portalpage set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select project.*,lead as lead_before,'<NEW_PD_VALUE>' as lead_after from project where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update project set lead = '<NEW_PD_VALUE>' where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : project-role mapping
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/roles where PROJECT_KEY: select pkey from project where id = ${pid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Users and roles'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project/{projectIdOrKey}/role
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectroleactor.*,roletypeparameter as roletypeparameter_before,'<NEW_PD_VALUE>' as roletypeparameter_after from projectroleactor where (LOWER(roletypeparameter) = LOWER('<CURRENT_PD_VALUE>') ) AND roletype  = 'atlassian-user-role-actor';

-- + UPDATE (be careful)
update projectroleactor set roletypeparameter = '<NEW_PD_VALUE>' where (LOWER(roletypeparameter) = LOWER('<CURRENT_PD_VALUE>') ) AND roletype  = 'atlassian-user-role-actor';

-- Type        : update
-- Origin      : jira
-- Description : used to auto log in based on cookie
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/UserRememberMeCookies!default.jspa?name=${username}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click 'Full name' value under specific user name
--     5. Button 'Actions' (top right)
--     6. Choose 'Remember My Login' Tokens
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remembermetoken.*,username as username_before,'<NEW_PD_VALUE>' as username_after from remembermetoken where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update remembermetoken set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,authorname as authorname_before,'<NEW_PD_VALUE>' as authorname_after from searchrequest where LOWER(authorname) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update searchrequest set authorname = '<NEW_PD_VALUE>' where LOWER(authorname) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,username as username_before,'<NEW_PD_VALUE>' as username_after from searchrequest where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update searchrequest set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : issue votes/watchers
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select userassociation.*,source_name as source_name_before,'<NEW_PD_VALUE>' as source_name_after from userassociation where LOWER(source_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update userassociation set source_name = '<NEW_PD_VALUE>' where LOWER(source_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select userhistoryitem.*,entityid as entityid_before,'<NEW_PD_VALUE>' as entityid_after from userhistoryitem where (LOWER(entityid) = LOWER('<CURRENT_PD_VALUE>') ) AND entitytype  IN ('UsedUser','Assignee');

-- + UPDATE (be careful)
update userhistoryitem set entityid = '<NEW_PD_VALUE>' where (LOWER(entityid) = LOWER('<CURRENT_PD_VALUE>') ) AND entitytype  IN ('UsedUser','Assignee');

-- Type        : update
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select userhistoryitem.*,username as username_before,'<NEW_PD_VALUE>' as username_after from userhistoryitem where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update userhistoryitem set username = '<NEW_PD_VALUE>' where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : worklog
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select worklog.*,author as author_before,'<NEW_PD_VALUE>' as author_after from worklog where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update worklog set author = '<NEW_PD_VALUE>' where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : update
-- Origin      : jira
-- Description : worklog
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select worklog.*,updateauthor as updateauthor_before,'<NEW_PD_VALUE>' as updateauthor_after from worklog where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;

-- + UPDATE (be careful)
update worklog set updateauthor = '<NEW_PD_VALUE>' where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;


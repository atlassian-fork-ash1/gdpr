-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') as DESCRIPTION_after from AO_A0B856_WEB_HOOK_LISTENER_AO where DESCRIPTION like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set DESCRIPTION = replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,NAME as NAME_before,replace(NAME, '<OLD_VALUE>', '<NEW_VALUE>') as NAME_after from AO_A0B856_WEB_HOOK_LISTENER_AO where NAME like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set NAME = replace(NAME, '<OLD_VALUE>', '<NEW_VALUE>') where NAME like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,URL as URL_before,'<NEW_VALUE>' as URL_after from AO_A0B856_WEB_HOOK_LISTENER_AO where URL like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set URL = '<NEW_VALUE>' where URL like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,VALUE as VALUE_before,replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as VALUE_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set VALUE = replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,copy_pattern as copy_pattern_before,replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') as copy_pattern_after from artifact_definition where copy_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set copy_pattern = replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') where copy_pattern like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from artifact_definition where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,src_directory as src_directory_before,replace(src_directory, '<OLD_VALUE>', '<NEW_VALUE>') as src_directory_after from artifact_definition where src_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set src_directory = replace(src_directory, '<OLD_VALUE>', '<NEW_VALUE>') where src_directory like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from artifact where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_subscription.*,dst_directory as dst_directory_before,replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') as dst_directory_after from artifact_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_subscription set dst_directory = replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') where dst_directory like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,msg as msg_before,replace(msg, '<OLD_VALUE>', '<NEW_VALUE>') as msg_after from audit_log where msg like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set msg = replace(msg, '<OLD_VALUE>', '<NEW_VALUE>') where msg like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,new_value as new_value_before,replace(new_value, '<OLD_VALUE>', '<NEW_VALUE>') as new_value_after from audit_log where new_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set new_value = replace(new_value, '<OLD_VALUE>', '<NEW_VALUE>') where new_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,old_value as old_value_before,replace(old_value, '<OLD_VALUE>', '<NEW_VALUE>') as old_value_after from audit_log where old_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set old_value = replace(old_value, '<OLD_VALUE>', '<NEW_VALUE>') where old_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,task_header as task_header_before,replace(task_header, '<OLD_VALUE>', '<NEW_VALUE>') as task_header_after from audit_log where task_header like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set task_header = replace(task_header, '<OLD_VALUE>', '<NEW_VALUE>') where task_header like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_email as author_email_before,'<NEW_VALUE>' as author_email_after from author where author_email = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update author set author_email = '<NEW_VALUE>' where author_email = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_name as author_name_before,replace(author_name, '<OLD_VALUE>', '<NEW_VALUE>') as author_name_after from author where author_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update author set author_name = replace(author_name, '<OLD_VALUE>', '<NEW_VALUE>') where author_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select bandana.*,serialized_data as serialized_data_before,replace(serialized_data, '<OLD_VALUE>', '<NEW_VALUE>') as serialized_data_after from bandana where serialized_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update bandana set serialized_data = replace(serialized_data, '<OLD_VALUE>', '<NEW_VALUE>') where serialized_data like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select brs_consumed_subscription.*,dst_directory as dst_directory_before,replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') as dst_directory_after from brs_consumed_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update brs_consumed_subscription set dst_directory = replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') where dst_directory like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build_definition.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from build_definition where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build_definition set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from build where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from build where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') as custom_info_value_after from buildresultsummary_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update buildresultsummary_customdata set custom_info_value = replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') where custom_info_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select capability.*,value as value_before,replace(value, '<OLD_VALUE>', '<NEW_VALUE>') as value_after from capability where value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update capability set value = replace(value, '<OLD_VALUE>', '<NEW_VALUE>') where value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from chain_stage where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from chain_stage where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from chain_stage_result where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage_result set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from chain_stage_result where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage_result set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from credentials where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update credentials set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,xml as xml_before,replace(xml, '<OLD_VALUE>', '<NEW_VALUE>') as xml_after from credentials where xml like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update credentials set xml = replace(xml, '<OLD_VALUE>', '<NEW_VALUE>') where xml like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,alias_name as alias_name_before,replace(alias_name, '<OLD_VALUE>', '<NEW_VALUE>') as alias_name_after from cwd_application_alias where alias_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application_alias set alias_name = replace(alias_name, '<OLD_VALUE>', '<NEW_VALUE>') where alias_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,lower_alias_name as lower_alias_name_before,replace(lower_alias_name, '<OLD_VALUE>', '<NEW_VALUE>') as lower_alias_name_after from cwd_application_alias where lower_alias_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application_alias set lower_alias_name = replace(lower_alias_name, '<OLD_VALUE>', '<NEW_VALUE>') where lower_alias_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_application where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_directory.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_directory where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_directory set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_expirable_user_token where email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set email_address = '<NEW_VALUE>' where email_address = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_group where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_group set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_user where email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set email_address = '<NEW_VALUE>' where email_address = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_email_address as lower_email_address_before,'<NEW_VALUE>' as lower_email_address_after from cwd_user where lower_email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_email_address = '<NEW_VALUE>' where lower_email_address = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,docker_pipeline_config as docker_pipeline_config_before,replace(docker_pipeline_config, '<OLD_VALUE>', '<NEW_VALUE>') as docker_pipeline_config_after from deployment_env_config where docker_pipeline_config like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_env_config set docker_pipeline_config = replace(docker_pipeline_config, '<OLD_VALUE>', '<NEW_VALUE>') where docker_pipeline_config like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,plugin_config as plugin_config_before,replace(plugin_config, '<OLD_VALUE>', '<NEW_VALUE>') as plugin_config_after from deployment_env_config where plugin_config like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_env_config set plugin_config = replace(plugin_config, '<OLD_VALUE>', '<NEW_VALUE>') where plugin_config like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from deployment_environment where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_environment where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,triggers_xml_data as triggers_xml_data_before,replace(triggers_xml_data, '<OLD_VALUE>', '<NEW_VALUE>') as triggers_xml_data_after from deployment_environment where triggers_xml_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set triggers_xml_data = replace(triggers_xml_data, '<OLD_VALUE>', '<NEW_VALUE>') where triggers_xml_data like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from deployment_environment where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from deployment_project where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project_item.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_project_item where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project_item set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_project where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_result_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') as custom_info_value_after from deployment_result_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_result_customdata set custom_info_value = replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') where custom_info_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_result.*,version_name as version_name_before,replace(version_name, '<OLD_VALUE>', '<NEW_VALUE>') as version_name_after from deployment_result where version_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_result set version_name = replace(version_name, '<OLD_VALUE>', '<NEW_VALUE>') where version_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from deployment_variable_substitution where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from deployment_variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') as commit_comment_clob_after from deployment_version_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_commit set commit_comment_clob = replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') where commit_comment_clob like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,copy_pattern as copy_pattern_before,replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') as copy_pattern_after from deployment_version_item_ba where copy_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set copy_pattern = replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') where copy_pattern like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from deployment_version_item_ba where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,location as location_before,replace(location, '<OLD_VALUE>', '<NEW_VALUE>') as location_after from deployment_version_item_ba where location like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set location = replace(location, '<OLD_VALUE>', '<NEW_VALUE>') where location like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_version_item where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_version where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_naming.*,next_version_name as next_version_name_before,replace(next_version_name, '<OLD_VALUE>', '<NEW_VALUE>') as next_version_name_after from deployment_version_naming where next_version_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_naming set next_version_name = replace(next_version_name, '<OLD_VALUE>', '<NEW_VALUE>') where next_version_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,plan_branch_name as plan_branch_name_before,replace(plan_branch_name, '<OLD_VALUE>', '<NEW_VALUE>') as plan_branch_name_after from deployment_version where plan_branch_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version set plan_branch_name = replace(plan_branch_name, '<OLD_VALUE>', '<NEW_VALUE>') where plan_branch_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from elastic_image where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update elastic_image set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from elastic_image where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update elastic_image set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select groups.*,groupname as groupname_before,replace(groupname, '<OLD_VALUE>', '<NEW_VALUE>') as groupname_after from groups where groupname like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update groups set groupname = replace(groupname, '<OLD_VALUE>', '<NEW_VALUE>') where groupname like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,host as host_before,replace(host, '<OLD_VALUE>', '<NEW_VALUE>') as host_after from imserver where host like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set host = replace(host, '<OLD_VALUE>', '<NEW_VALUE>') where host like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,resource_name as resource_name_before,replace(resource_name, '<OLD_VALUE>', '<NEW_VALUE>') as resource_name_after from imserver where resource_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set resource_name = replace(resource_name, '<OLD_VALUE>', '<NEW_VALUE>') where resource_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from imserver where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select label.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from label where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update label set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select merge_result.*,failure_reason as failure_reason_before,replace(failure_reason, '<OLD_VALUE>', '<NEW_VALUE>') as failure_reason_after from merge_result where failure_reason like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update merge_result set failure_reason = replace(failure_reason, '<OLD_VALUE>', '<NEW_VALUE>') where failure_reason like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,replace(recipient, '<OLD_VALUE>', '<NEW_VALUE>') as recipient_after from notifications where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
update notifications set recipient = replace(recipient, '<OLD_VALUE>', '<NEW_VALUE>') where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_key as entity_key_before,replace(entity_key, '<OLD_VALUE>', '<NEW_VALUE>') as entity_key_after from os_propertyentry where entity_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set entity_key = replace(entity_key, '<OLD_VALUE>', '<NEW_VALUE>') where entity_key like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_name as entity_name_before,replace(entity_name, '<OLD_VALUE>', '<NEW_VALUE>') as entity_name_after from os_propertyentry where entity_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set entity_name = replace(entity_name, '<OLD_VALUE>', '<NEW_VALUE>') where entity_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,string_val as string_val_before,replace(string_val, '<OLD_VALUE>', '<NEW_VALUE>') as string_val_after from os_propertyentry where string_val like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set string_val = replace(string_val, '<OLD_VALUE>', '<NEW_VALUE>') where string_val like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select plan_vcs_history.*,xml_custom_data as xml_custom_data_before,replace(xml_custom_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_custom_data_after from plan_vcs_history where xml_custom_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update plan_vcs_history set xml_custom_data = replace(xml_custom_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_custom_data like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select project.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from project where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update project set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select project.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from project where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update project set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select queue.*,agent_description as agent_description_before,replace(agent_description, '<OLD_VALUE>', '<NEW_VALUE>') as agent_description_after from queue where agent_description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update queue set agent_description = replace(agent_description, '<OLD_VALUE>', '<NEW_VALUE>') where agent_description like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select queue.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from queue where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update queue set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,configuration as configuration_before,replace(configuration, '<OLD_VALUE>', '<NEW_VALUE>') as configuration_after from quick_filter_rules where configuration like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filter_rules set configuration = replace(configuration, '<OLD_VALUE>', '<NEW_VALUE>') where configuration like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from quick_filter_rules where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filter_rules set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filters.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from quick_filters where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filters set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,key_identifier as key_identifier_before,replace(key_identifier, '<OLD_VALUE>', '<NEW_VALUE>') as key_identifier_after from requirement where key_identifier like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update requirement set key_identifier = replace(key_identifier, '<OLD_VALUE>', '<NEW_VALUE>') where key_identifier like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,match_value as match_value_before,replace(match_value, '<OLD_VALUE>', '<NEW_VALUE>') as match_value_after from requirement where match_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update requirement set match_value = replace(match_value, '<OLD_VALUE>', '<NEW_VALUE>') where match_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select script.*,body as body_before,replace(body, '<OLD_VALUE>', '<NEW_VALUE>') as body_after from script where body like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update script set body = replace(body, '<OLD_VALUE>', '<NEW_VALUE>') where body like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select stage_variable_context.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from stage_variable_context where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update stage_variable_context set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select stage_variable_context.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from stage_variable_context where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update stage_variable_context set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select test_error.*,error_content as error_content_before,replace(error_content, '<OLD_VALUE>', '<NEW_VALUE>') as error_content_after from test_error where error_content like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update test_error set error_content = replace(error_content, '<OLD_VALUE>', '<NEW_VALUE>') where error_content like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps.*,app_name as app_name_before,replace(app_name, '<OLD_VALUE>', '<NEW_VALUE>') as app_name_after from trusted_apps where app_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update trusted_apps set app_name = replace(app_name, '<OLD_VALUE>', '<NEW_VALUE>') where app_name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_ips.*,ip_pattern as ip_pattern_before,'<NEW_VALUE>' as ip_pattern_after from trusted_apps_ips where ip_pattern = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update trusted_apps_ips set ip_pattern = '<NEW_VALUE>' where ip_pattern = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_urls.*,url_pattern as url_pattern_before,'<NEW_VALUE>' as url_pattern_after from trusted_apps_urls where url_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update trusted_apps_urls set url_pattern = '<NEW_VALUE>' where url_pattern like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,content as content_before,replace(content, '<OLD_VALUE>', '<NEW_VALUE>') as content_after from user_comment where content like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update user_comment set content = replace(content, '<OLD_VALUE>', '<NEW_VALUE>') where content like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') as commit_comment_clob_after from user_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update user_commit set commit_comment_clob = replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') where commit_comment_clob like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select users.*,email as email_before,'<NEW_VALUE>' as email_after from users where email = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update users set email = '<NEW_VALUE>' where email = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select users.*,fullname as fullname_before,replace(fullname, '<OLD_VALUE>', '<NEW_VALUE>') as fullname_after from users where fullname like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update users set fullname = replace(fullname, '<OLD_VALUE>', '<NEW_VALUE>') where fullname like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from variable_context where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_context set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from variable_context where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_context set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from variable_substitution where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_substitution set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_substitution set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from vcs_location where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update vcs_location set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from vcs_location where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update vcs_location set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;


-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : postgresql

select "test_error".* from test_error where LOWER(error_content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

select "credentials".* from credentials where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


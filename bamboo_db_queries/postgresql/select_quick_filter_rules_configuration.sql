-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "quick_filter_rules".* from quick_filter_rules where LOWER(configuration) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "artifact_definition".*,src_directory as src_directory_before,REGEXP_REPLACE(src_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as src_directory_after from artifact_definition where LOWER(src_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update artifact_definition set src_directory = REGEXP_REPLACE(src_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(src_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(plan_branch_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


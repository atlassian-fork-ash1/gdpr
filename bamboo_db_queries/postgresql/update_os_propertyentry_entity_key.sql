-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "os_propertyentry".*,entity_key as entity_key_before,REGEXP_REPLACE(entity_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as entity_key_after from os_propertyentry where LOWER(entity_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update os_propertyentry set entity_key = REGEXP_REPLACE(entity_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(entity_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


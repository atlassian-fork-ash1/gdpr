-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : postgresql

select "capability".* from capability where LOWER(value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


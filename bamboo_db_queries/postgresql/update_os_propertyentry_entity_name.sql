-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "os_propertyentry".*,entity_name as entity_name_before,REGEXP_REPLACE(entity_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as entity_name_after from os_propertyentry where LOWER(entity_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update os_propertyentry set entity_name = REGEXP_REPLACE(entity_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(entity_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : postgresql

select "deployment_project".* from deployment_project where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : postgresql

select "label".* from label where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : postgresql

select "author".* from author where LOWER(author_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


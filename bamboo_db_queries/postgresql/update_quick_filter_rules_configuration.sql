-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "quick_filter_rules".*,configuration as configuration_before,REGEXP_REPLACE(configuration,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as configuration_after from quick_filter_rules where LOWER(configuration) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update quick_filter_rules set configuration = REGEXP_REPLACE(configuration,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(configuration) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


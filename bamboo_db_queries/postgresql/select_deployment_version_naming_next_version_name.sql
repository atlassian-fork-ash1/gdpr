-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : postgresql

select "deployment_version_naming".* from deployment_version_naming where LOWER(next_version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


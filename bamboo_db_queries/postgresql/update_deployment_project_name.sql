-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_project".*,name as name_before,REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as name_after from deployment_project where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_project set name = REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


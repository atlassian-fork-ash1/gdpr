-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "os_propertyentry".*,string_val as string_val_before,REGEXP_REPLACE(string_val,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as string_val_after from os_propertyentry where LOWER(string_val) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update os_propertyentry set string_val = REGEXP_REPLACE(string_val,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(string_val) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


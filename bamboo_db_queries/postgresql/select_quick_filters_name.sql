-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "quick_filters".* from quick_filters where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : postgresql

select "deployment_env_config".* from deployment_env_config where LOWER(plugin_config) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


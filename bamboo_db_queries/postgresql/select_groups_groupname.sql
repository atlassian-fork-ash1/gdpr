-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : postgresql

select "groups".* from groups where LOWER(groupname) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


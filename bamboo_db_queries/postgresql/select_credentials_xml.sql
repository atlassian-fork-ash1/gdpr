-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

select "credentials".* from credentials where LOWER(xml) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


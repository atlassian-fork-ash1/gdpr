-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : postgresql

select "deployment_project_item".* from deployment_project_item where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


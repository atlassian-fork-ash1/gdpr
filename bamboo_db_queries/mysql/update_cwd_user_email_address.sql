-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_user where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set email_address = '<NEW_VALUE>' where LOWER(email_address) = LOWER('<OLD_VALUE>') ;


-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_result.*,version_name as version_name_before,replace(version_name,'<OLD_VALUE>','<NEW_VALUE>') as version_name_after from deployment_result where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_result set version_name = replace(version_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


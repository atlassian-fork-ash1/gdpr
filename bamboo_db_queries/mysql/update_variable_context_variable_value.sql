-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_value as variable_value_before,replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') as variable_value_after from variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_context set variable_value = replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


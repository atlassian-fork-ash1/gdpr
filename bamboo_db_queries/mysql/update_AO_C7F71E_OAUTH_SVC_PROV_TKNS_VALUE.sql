-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,VALUE as VALUE_before,replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') as VALUE_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set VALUE = replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


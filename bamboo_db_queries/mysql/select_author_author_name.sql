-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

select author.* from author where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mysql

select deployment_version_item.* from deployment_version_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


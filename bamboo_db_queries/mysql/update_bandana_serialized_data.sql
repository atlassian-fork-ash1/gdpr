-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select bandana.*,serialized_data as serialized_data_before,replace(serialized_data,'<OLD_VALUE>','<NEW_VALUE>') as serialized_data_after from bandana where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update bandana set serialized_data = replace(serialized_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


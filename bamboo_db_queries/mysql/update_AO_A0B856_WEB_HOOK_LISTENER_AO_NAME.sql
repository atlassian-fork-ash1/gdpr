-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,NAME as NAME_before,replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') as NAME_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_A0B856_WEB_HOOK_LISTENER_AO set NAME = replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


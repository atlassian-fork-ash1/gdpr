-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,parent_name as parent_name_before,'<NEW_VALUE>' as parent_name_after from cwd_membership where LOWER(parent_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_membership set parent_name = '<NEW_VALUE>' where LOWER(parent_name) = LOWER('<OLD_VALUE>') ;


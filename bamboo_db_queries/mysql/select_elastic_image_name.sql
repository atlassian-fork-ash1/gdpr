-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

select elastic_image.* from elastic_image where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


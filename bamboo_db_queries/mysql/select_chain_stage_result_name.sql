-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

select chain_stage_result.* from chain_stage_result where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


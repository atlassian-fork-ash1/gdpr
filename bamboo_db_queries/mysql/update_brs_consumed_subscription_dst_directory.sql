-- Type        : update
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select brs_consumed_subscription.*,dst_directory as dst_directory_before,replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') as dst_directory_after from brs_consumed_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update brs_consumed_subscription set dst_directory = replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


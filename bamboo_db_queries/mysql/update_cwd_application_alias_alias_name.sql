-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,alias_name as alias_name_before,replace(alias_name,'<OLD_VALUE>','<NEW_VALUE>') as alias_name_after from cwd_application_alias where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_application_alias set alias_name = replace(alias_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


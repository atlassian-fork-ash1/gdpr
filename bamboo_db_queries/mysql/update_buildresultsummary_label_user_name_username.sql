-- Type        : update
-- Origin      : bamboo
-- Description : creator of result label
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_label.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from buildresultsummary_label where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update buildresultsummary_label set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;


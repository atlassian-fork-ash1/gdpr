-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mysql

select build.* from build where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mysql

select artifact_subscription.* from artifact_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


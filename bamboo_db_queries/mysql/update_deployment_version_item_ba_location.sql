-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,location as location_before,replace(location,'<OLD_VALUE>','<NEW_VALUE>') as location_after from deployment_version_item_ba where LOWER(location) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item_ba set location = replace(location,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(location) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


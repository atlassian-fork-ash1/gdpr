-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_F1B80E_BRANCH_SELECTION.*,USER as USER_before,'<NEW_VALUE>' as USER_after from AO_F1B80E_BRANCH_SELECTION where LOWER(USER) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_F1B80E_BRANCH_SELECTION set USER = '<NEW_VALUE>' where LOWER(USER) = LOWER('<OLD_VALUE>') ;


-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select users.*,fullname as fullname_before,replace(fullname,'<OLD_VALUE>','<NEW_VALUE>') as fullname_after from users where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update users set fullname = replace(fullname,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


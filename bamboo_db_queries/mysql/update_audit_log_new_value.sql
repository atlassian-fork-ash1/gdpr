-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,new_value as new_value_before,replace(new_value,'<OLD_VALUE>','<NEW_VALUE>') as new_value_after from audit_log where LOWER(new_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set new_value = replace(new_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(new_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

select credentials.* from credentials where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


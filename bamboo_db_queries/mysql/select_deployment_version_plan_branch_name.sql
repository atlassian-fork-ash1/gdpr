-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

select deployment_version.* from deployment_version where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';


-- Type        : update
-- Origin      : bamboo
-- Description : External users name
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select external_entities.*,name as name_before,'<NEW_VALUE>' as name_after from external_entities where name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update external_entities set name = '<NEW_VALUE>' where name = '<OLD_VALUE>' ;


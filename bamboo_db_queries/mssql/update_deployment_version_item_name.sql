-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_version_item where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;


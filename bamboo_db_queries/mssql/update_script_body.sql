-- Type        : update
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select script.*,body as body_before,replace(body, '<OLD_VALUE>', '<NEW_VALUE>') as body_after from script where body like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update script set body = replace(body, '<OLD_VALUE>', '<NEW_VALUE>') where body like '%<OLD_VALUE>%' ;


-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,VALUE as VALUE_before,replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as VALUE_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set VALUE = replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where VALUE like '%<OLD_VALUE>%' ;


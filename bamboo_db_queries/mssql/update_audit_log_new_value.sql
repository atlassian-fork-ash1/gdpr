-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,new_value as new_value_before,replace(new_value, '<OLD_VALUE>', '<NEW_VALUE>') as new_value_after from audit_log where new_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set new_value = replace(new_value, '<OLD_VALUE>', '<NEW_VALUE>') where new_value like '%<OLD_VALUE>%' ;


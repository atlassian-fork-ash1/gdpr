-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from chain_stage_result where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage_result set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;


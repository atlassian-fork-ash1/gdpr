-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,location as location_before,replace(location, '<OLD_VALUE>', '<NEW_VALUE>') as location_after from deployment_version_item_ba where location like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set location = replace(location, '<OLD_VALUE>', '<NEW_VALUE>') where location like '%<OLD_VALUE>%' ;


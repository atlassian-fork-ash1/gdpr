-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select groups.*,groupname as groupname_before,replace(groupname, '<OLD_VALUE>', '<NEW_VALUE>') as groupname_after from groups where groupname like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update groups set groupname = replace(groupname, '<OLD_VALUE>', '<NEW_VALUE>') where groupname like '%<OLD_VALUE>%' ;


-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select project.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from project where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update project set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;


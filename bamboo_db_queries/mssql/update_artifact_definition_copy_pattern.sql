-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,copy_pattern as copy_pattern_before,replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') as copy_pattern_after from artifact_definition where copy_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set copy_pattern = replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') where copy_pattern like '%<OLD_VALUE>%' ;


-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from quick_filter_rules where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filter_rules set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;


-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_granted_perm.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_granted_perm where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_granted_perm set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;


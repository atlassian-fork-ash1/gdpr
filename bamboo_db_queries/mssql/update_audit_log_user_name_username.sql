-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from audit_log where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update audit_log set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;


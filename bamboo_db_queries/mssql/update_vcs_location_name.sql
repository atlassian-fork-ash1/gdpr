-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from vcs_location where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update vcs_location set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;


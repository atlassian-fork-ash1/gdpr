-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mssql

select deployment_version_naming.* from deployment_version_naming where next_version_name like '%<OLD_VALUE>%' ;


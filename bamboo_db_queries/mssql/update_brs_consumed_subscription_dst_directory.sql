-- Type        : update
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select brs_consumed_subscription.*,dst_directory as dst_directory_before,replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') as dst_directory_after from brs_consumed_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update brs_consumed_subscription set dst_directory = replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') where dst_directory like '%<OLD_VALUE>%' ;


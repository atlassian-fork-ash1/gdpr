-- Type        : update
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_subscription.*,dst_directory as dst_directory_before,replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') as dst_directory_after from artifact_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_subscription set dst_directory = replace(dst_directory, '<OLD_VALUE>', '<NEW_VALUE>') where dst_directory like '%<OLD_VALUE>%' ;


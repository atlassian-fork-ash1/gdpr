-- Type        : select
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : mssql

select password_reset_token.* from password_reset_token where username = '<OLD_VALUE>' ;


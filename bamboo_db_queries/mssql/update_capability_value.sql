-- Type        : update
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select capability.*,value as value_before,replace(value, '<OLD_VALUE>', '<NEW_VALUE>') as value_after from capability where value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update capability set value = replace(value, '<OLD_VALUE>', '<NEW_VALUE>') where value like '%<OLD_VALUE>%' ;


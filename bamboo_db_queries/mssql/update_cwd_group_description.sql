-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_group where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_group set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;


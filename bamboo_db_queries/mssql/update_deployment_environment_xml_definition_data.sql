-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from deployment_environment where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;


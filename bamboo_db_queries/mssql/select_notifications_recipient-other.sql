-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

select notifications.* from notifications where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';


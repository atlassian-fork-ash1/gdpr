-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from build where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;


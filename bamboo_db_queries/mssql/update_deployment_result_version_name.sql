-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_result.*,version_name as version_name_before,replace(version_name, '<OLD_VALUE>', '<NEW_VALUE>') as version_name_after from deployment_result where version_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_result set version_name = replace(version_name, '<OLD_VALUE>', '<NEW_VALUE>') where version_name like '%<OLD_VALUE>%' ;


-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from user_comment where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update user_comment set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;


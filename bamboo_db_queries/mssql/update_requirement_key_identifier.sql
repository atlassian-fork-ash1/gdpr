-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,key_identifier as key_identifier_before,replace(key_identifier, '<OLD_VALUE>', '<NEW_VALUE>') as key_identifier_after from requirement where key_identifier like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update requirement set key_identifier = replace(key_identifier, '<OLD_VALUE>', '<NEW_VALUE>') where key_identifier like '%<OLD_VALUE>%' ;


-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(triggers_xml_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


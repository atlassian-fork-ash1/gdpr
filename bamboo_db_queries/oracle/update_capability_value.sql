-- Type        : update
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select capability.*,value as value_before,REGEXP_REPLACE(value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as value_after from capability where REGEXP_LIKE (LOWER(value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update capability set value = REGEXP_REPLACE(value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(host),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


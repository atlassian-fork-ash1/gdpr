-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select groups.*,groupname as groupname_before,REGEXP_REPLACE(groupname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as groupname_after from groups where REGEXP_LIKE (LOWER(groupname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update groups set groupname = REGEXP_REPLACE(groupname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(groupname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


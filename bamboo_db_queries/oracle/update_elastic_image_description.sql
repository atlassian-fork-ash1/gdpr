-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,description as description_before,REGEXP_REPLACE(description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as description_after from elastic_image where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update elastic_image set description = REGEXP_REPLACE(description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


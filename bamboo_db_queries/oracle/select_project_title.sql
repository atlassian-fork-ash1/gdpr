-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : oracle

select project.* from project where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,configuration as configuration_before,REGEXP_REPLACE(configuration,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as configuration_after from quick_filter_rules where REGEXP_LIKE (LOWER(configuration),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update quick_filter_rules set configuration = REGEXP_REPLACE(configuration,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(configuration),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(msg),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


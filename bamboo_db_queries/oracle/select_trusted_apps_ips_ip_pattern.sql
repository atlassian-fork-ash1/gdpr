-- Type        : select
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : oracle

select trusted_apps_ips.* from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;


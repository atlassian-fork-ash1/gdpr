-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : oracle

select capability.* from capability where REGEXP_LIKE (LOWER(value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


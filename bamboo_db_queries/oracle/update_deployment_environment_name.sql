-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,name as name_before,REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as name_after from deployment_environment where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update deployment_environment set name = REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


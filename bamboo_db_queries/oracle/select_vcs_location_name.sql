-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : oracle

select vcs_location.* from vcs_location where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,docker_pipeline_config as docker_pipeline_config_before,REGEXP_REPLACE(docker_pipeline_config,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as docker_pipeline_config_after from deployment_env_config where REGEXP_LIKE (LOWER(docker_pipeline_config),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update deployment_env_config set docker_pipeline_config = REGEXP_REPLACE(docker_pipeline_config,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(docker_pipeline_config),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : oracle

select script.* from script where REGEXP_LIKE (LOWER(body),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


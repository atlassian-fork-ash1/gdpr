-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select imserver.*,resource_name as resource_name_before,REGEXP_REPLACE(resource_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as resource_name_after from imserver where REGEXP_LIKE (LOWER(resource_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update imserver set resource_name = REGEXP_REPLACE(resource_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(resource_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


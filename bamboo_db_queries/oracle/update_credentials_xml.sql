-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select credentials.*,xml as xml_before,REGEXP_REPLACE(xml,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as xml_after from credentials where REGEXP_LIKE (LOWER(xml),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update credentials set xml = REGEXP_REPLACE(xml,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(xml),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


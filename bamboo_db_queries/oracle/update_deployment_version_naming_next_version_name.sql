-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_version_naming.*,next_version_name as next_version_name_before,REGEXP_REPLACE(next_version_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as next_version_name_after from deployment_version_naming where REGEXP_LIKE (LOWER(next_version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update deployment_version_naming set next_version_name = REGEXP_REPLACE(next_version_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(next_version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');


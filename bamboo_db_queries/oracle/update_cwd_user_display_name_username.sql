-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,display_name as display_name_before,'<NEW_VALUE>' as display_name_after from cwd_user where LOWER(display_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set display_name = '<NEW_VALUE>' where LOWER(display_name) = LOWER('<OLD_VALUE>') ;


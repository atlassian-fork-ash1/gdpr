-- Type       : update
-- Origin     : User Mapping
-- Description: Update User Mapping
update user_mapping set username = user_mapping.user_key, lower_username = user_mapping.user_key where user_key IN (SELECT * FROM (select user_key from user_mapping where username = '__username__' ) AS reference);
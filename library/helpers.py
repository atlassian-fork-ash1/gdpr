#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
__author__      = "Tomasz Dudzisz <tdudzisz@atlassian.com>"
__copyright__   = "Copyright 2018, Atlassian.com"
__credits__     = []
__license__     = "BSD"
__version__     = "1.0.1"
__status__      = "Development"
__created__     = 18/04/2018
__updated__     = 18/04/2018

__project__     = gdpr
__file__        = helpers.py
__description__ = 

"""

import sys, os


if __name__ == '__main__':
    pass


class Database:
    MYSQL = "mysql"
    MSSQL = "mssql"
    ORACLE = "oracle"
    POSTGRESQL = "postgresql"

class Storage:
    EXTREMELY_LONG = "extremely-long"
    VERY_LONG = "very-long"

class Generic():

    def __init__(self):

        self.where = "where"
        self.action = "action"
        self.origin = "origin"
        self.type = "type"
        self.description = "description"
        self.active = "active"

        self.api = "api"
        self.steps = "steps"

        self.db_mysql = Database.MYSQL
        self.db_mssql = Database.MSSQL
        self.db_oracle = Database.ORACLE
        self.db_postgresql = Database.POSTGRESQL

        self.database = None

        self.output_path = None
        self.user_order = False
        self.order = 0

        self.comments = None


    @staticmethod
    def get_filename(action, table, column_name, column_type, user_key_name, user_key_postfix):

        filename = "{}_{}_{}".format(action, table, column_name)

        if column_type == user_key_name:
            filename = ("{}_" + user_key_postfix + ".sql").format(filename)
        else:
            filename = "{}.sql".format(filename)

        return filename


    def save_for_db(self, database, table, column_name, column_data, action, sql, user_key_name='user_key', user_key_postfix='userkey'):

        """
        format query for database type
        """

        self.comments.append("-- Database    : {}\n".format(database))

        data = self.comments[:]
        data.append(sql)

        data = "\n".join(data)

        filename = Generic.get_filename(action, table, column_name, column_data['type'], user_key_name, user_key_postfix)

        self.save_to_file(database, filename, data, action)


    def get_order(self):
        if self.user_order:

            if self.order < 10:
                order = "0{}_".format(self.order)
            else:
                order = "{}_".format(self.order)
            return order

        else:
            return ""


    def save_to_file(self, database, filename, data, action):
        try:

            path = "{}/{}/{}".format(self.output_path, database, filename)

            if self.user_order:
                path = "{}/{}/{}_{}".format(self.output_path, database, self.get_order(), filename)

            f = open(path, "w")
            f.write(data)
            f.close()

        except Exception as e:
            print("Error saving file: {}".format(filename))
            raise Exception(e)


    def clear_output(self):

        """
        1. create output dir if doesn't exist
        2. remove a dir content in exists
        """

        path = "{}/{}".format(self.output_path, self.database)
        self.recreate_dir(path)


    def recreate_dir(self, path):

        if os.path.exists(path):
            print("Output directory found, cleaning: {}".format(path))
            for the_file in os.listdir(path):
                file_path = os.path.join(path, the_file)
                try:
                    if os.path.isfile(file_path):
                        print("Removing: {}".format(file_path))
                        os.unlink(file_path)
                except Exception as e:
                    print(e)
        else:
            print("Creating output dir: {}".format(self.output_path))
            os.makedirs(path, exist_ok = True)

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_0201F0_STATS_EVENT_PARAM.* from AO_0201F0_STATS_EVENT_PARAM where LOWER(PARAM_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_2C4E5C_MAILCHANNEL.* from AO_2C4E5C_MAILCHANNEL where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_2C4E5C_MAILCHANNEL.* from AO_2C4E5C_MAILCHANNEL where LOWER(MODIFIED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILCONNECTION.* from AO_2C4E5C_MAILCONNECTION where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILCONNECTION.* from AO_2C4E5C_MAILCONNECTION where LOWER(USER_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where LOWER(FROM_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels using a certain email account
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_EMAILCHANNELSETTING.* from AO_54307E_EMAILCHANNELSETTING where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Organisation members
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_ORGANIZATION_MEMBER.* from AO_54307E_ORGANIZATION_MEMBER where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_ORGANIZATION.* from AO_54307E_ORGANIZATION where LOWER(NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_ORGANIZATION.* from AO_54307E_ORGANIZATION where LOWER(SEARCH_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request subscriptions for user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_SUBSCRIPTION.* from AO_54307E_SUBSCRIPTION where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Approver decisions made by user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_56464C_APPROVERDECISION.* from AO_56464C_APPROVERDECISION where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_97EDAB_USERINVITATION.* from AO_97EDAB_USERINVITATION where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_97EDAB_USERINVITATION.* from AO_97EDAB_USERINVITATION where LOWER(SENDER_USERNAME) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_PROJECT_USER_CONTEXT.* from AO_9B2E3B_PROJECT_USER_CONTEXT where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation, automation executor
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_RULE_EXECUTION.* from AO_9B2E3B_RULE_EXECUTION where LOWER(EXECUTOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Notification templates
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_C7F17E_PROJECT_LANG_REV.* from AO_C7F17E_PROJECT_LANG_REV where LOWER(AUTHOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CANNEDRESPONSEAUDIT.* from AO_D530BB_CANNEDRESPONSEAUDIT where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response user usage
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CANNEDRESPONSEUSAGE.* from AO_D530BB_CANNEDRESPONSEUSAGE where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where LOWER(CREATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where LOWER(UPDATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

